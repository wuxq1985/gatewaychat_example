<?php
/**
 * run with command
 * php start.php start
 */
//检测开发环境开关
//ini_set('display_errors', 'on');

// 标记是全局启动
require_once __DIR__. '/vendor/autoload.php';
use GatewayChat\App;
$root=__DIR__.'/websocket_config/';
$route=$root.'route.php';
$config=$root.'config.php';
(new App($route,$config))->make()->run();
