<?php
declare (strict_types = 1);

namespace app\command;

use GatewayChat\ThinkWorker;
use think\App;
use think\Config;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;

/*!defined('GLOBAL_START')  && define('GLOBAL_START', 1);
!defined('ROOT_PATH')  && define('ROOT_PATH', app()->getRootPath());*/
class Chat extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('chat')
            ->addArgument('name', Argument::OPTIONAL, "your name")
            ->addOption('path', 'd', Option::VALUE_OPTIONAL, 'path to clear', null)
            ->setDescription('the chat command');        
    }

    protected function execute(Input $input, Output $output)
    {
        $root=app()->getRootPath().'/websocket_config/';
        $route=$root.'route.php';
        $config=$root.'config.php';
        (new \GatewayChat\App($route,$config))->make();
        ThinkWorker::runAll();
    }
}
