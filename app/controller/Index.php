<?php
namespace app\controller;


use app\BaseController;
use GatewayClient\Gateway;
use think\facade\View;

class Index extends BaseController
{
    public function index()
    {
        Gateway::$registerAddress = '127.0.0.1:8103';
        return json([
            '在线人员'=>Gateway::getAllClientCount(),
            '在线UI人员'=>Gateway::getAllUidList(),
            '在线组人员'=>Gateway::getAllGroupClientIdList(),
            '在线人员session'=>Gateway::getAllClientSessions()
        ]);
    }

    public function debug()
    {
        return View::fetch();
    }

    public function hello($name = 'ThinkPHP6')
    {
        return 'hello,' . $name;
    }
}
