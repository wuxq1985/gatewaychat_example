<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020-05-25
 * Time: 12:20
 */
namespace app\controller;

use app\BaseController;
use GatewayClient\Gateway;
use think\facade\View;
define('APPKEY',env('getui.appkey'));
define('APPID',env('getui.appid'));
define('MASTERSECRET',env('getui.mastersecret'));
define('HOST','http://sdk.open.api.igexin.com/apiex.htm');
class Ge extends BaseController
{
    public function index()
    {
//        echo  env('getui.appid'),PHP_EOL;
//        echo  env('getui.appkey'),PHP_EOL;
//        echo  env('getui.appsecret'),PHP_EOL;
//        echo  env('getui.mastersecret'),PHP_EOL;
        $igt = new \IGeTui(HOST,APPKEY,MASTERSECRET);
        $template = $this->IGtNotyPopLoadTemplateDemo();
        //定义"SingleMessage"
        $message = new \IGtSingleMessage();

        $message->set_isOffline(true);//是否离线
        $message->set_offlineExpireTime(3600*12*1000);//离线时间
        $message->set_data($template);//设置推送消息类型
        //$message->set_PushNetWorkType(0);//设置是否根据WIFI推送消息，2为4G/3G/2G，1为wifi推送，0为不限制推送
        //接收方
        $target = new \IGtTarget();
        $target->set_appId(APPID);
        $target->set_clientId('aa257523f10ebdd597a4a4046f033659');
        //    $target->set_alias(Alias);

        try {
            $rep = $igt->pushMessageToSingle($message, $target);
            var_dump($rep);
            echo ("<br><br>");

        }catch(\RequestException $e){
            $requstId =e.getRequestId();
            //失败时重发
            $rep = $igt->pushMessageToSingle($message, $target,$requstId);
            var_dump($rep);
            echo ("<br><br>");
        }
    }
    private function IGtNotyPopLoadTemplateDemo(){
        $template =  new \IGtNotyPopLoadTemplate();
        $template ->set_appId(APPID);                      //应用appid
        $template ->set_appkey(APPKEY);                    //应用appkey
        //通知栏
        $template ->set_notyTitle("请填写通知标题");                 //通知栏标题
        $template ->set_notyContent("请填写通知内容"); //通知栏内容
        $template ->set_notyIcon("");                      //通知栏logo
        $template ->set_isBelled(true);                    //是否响铃
        $template ->set_isVibrationed(true);               //是否震动
        $template ->set_isCleared(true);                   //通知栏是否可清除
        /*//弹框
        $template ->set_popTitle("弹框标题");              //弹框标题
        $template ->set_popContent("弹框内容");            //弹框内容
        $template ->set_popImage("");                      //弹框图片
        $template ->set_popButton1("下载");                //左键
        $template ->set_popButton2("取消");                //右键
        //下载
        $template ->set_loadIcon("");                      //弹框图片
        $template ->set_loadTitle("请填写下载标题");
        $template ->set_loadUrl("请填写下载地址");
        $template ->set_isAutoInstall(false);
        $template ->set_isActived(true);*/

        //设置通知定时展示时间，结束时间与开始时间相差需大于6分钟，消息推送后，客户端将在指定时间差内展示消息（误差6分钟）
        //$begin = "XXXX-XX-XX XX:XX:XX";
        //$end = "XXXX-XX-XX XX:XX:XX";
        //$template->set_duration($begin,$end);
        return $template;
    }
}