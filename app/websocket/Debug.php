<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020-05-13
 * Time: 19:03
 */
namespace app\websocket;
use GatewayChat\Contract\OnMessageInterface;
use GatewayChat\Contract\OnWebSocketConnectInterface;
use GatewayChat\Contract\OnWorkerInterface;
use GatewayWorker\BusinessWorker;
use GatewayWorker\Lib\Gateway;
use Workerman\Lib\Timer;
use Workerman\MySQL\Connection;

class Debug implements OnWebSocketConnectInterface,OnWorkerInterface
{

    public function  onWorkerStart(BusinessWorker $worker)
    {
        // TODO: Implement onWorkerStart() method.
        if($worker->id===1){
            Timer::add(10,function (){
                Timer::add(5,function (){
                    $on_line=Gateway::getAllUidList();
                    $on_line=array_values($on_line);
                    Gateway::sendToGroup('admin',json_encode($on_line,true));
                });
            },null,false);
        }

    }

    public function onWorkerStop(BusinessWorker $worker)
    {
        // TODO: Implement onWorkerStop() method.
    }

    public function onWebSocketConnect($client_id, $data, Connection $db, \Redis $redis)
    {
        // TODO: Implement onWebSocketConnect() method.
        Gateway::joinGroup($client_id,'admin');
        $on_line=Gateway::getAllUidList();
        $on_line=array_values($on_line);
        Gateway::sendToClient($client_id,json_encode($on_line,true));

    }
}