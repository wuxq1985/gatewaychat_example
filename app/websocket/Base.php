<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020-05-13
 * Time: 19:03
 */
namespace app\websocket;
use GatewayChat\Contract\OnMessageInterface;
use GatewayWorker\Lib\Gateway;

class Base implements OnMessageInterface
{
    public function onMessage($client_id, $message,$db,\Redis $redis)
    {
        Gateway::sendToClient($client_id,$message.'---'.self::class.'---调用结束');
    }
}