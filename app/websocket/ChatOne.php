<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020-05-13
 * Time: 19:03
 */
namespace app\websocket;
use GatewayWorker\Lib\Gateway;
use Workerman\MySQL\Connection;

class ChatOne
{
    public static function onWebSocketConnect($client_id,$data,Connection $db,\Redis $redis)
    {
        Gateway::bindUid($client_id,'1');
    }

    public static function onMessage($client_id, $message,$db,$redis)
    {
        //Gateway::sendToClient($client_id,$message.'+++++++++++');
        //var_dump(  Gateway::isUidOnline('1') );
    }

    public static function onClose($client_id,Connection $db,\Redis $redis)
    {
        //var_dump(  Gateway::isUidOnline('1') );
    }
}