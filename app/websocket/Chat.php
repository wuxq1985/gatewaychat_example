<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020-05-13
 * Time: 19:03
 */
namespace app\websocket;
use GatewayWorker\Lib\Gateway;

class Chat
{
    public static function onMessage($client_id, $message,$db,\Redis $redis)
    {
        $key='chat:online';
        if($message==='ping' && isset($_SESSION['id'])){
            $redis->lPush($key.$_SESSION['id'], time());
        }
        // Gateway::sendToClient($client_id,__CLASS__.'::'.__FUNCTION__.'收到消息');
        return true;
    }
}