<?php
return [
    'database'=>[
        // 服务器地址
        'hostname'          => '127.0.0.1',
        // 数据库名
        'database'          => 'chat',
        // 用户名
        'username'          => 'daixiaozhan',
        // 密码
        'password'          => 'Aa123456',
        // 端口
        'hostport'          => 3306,

    ],
    'websocket'=>
    [
        'servers'=>[
            [
                /*以下数据为必改数据*/
                'name'=>'chat_001',
                //外部连接端口
                'port'=>8201,
                //内部通信端口
                'bus_port'=>8101,
                //假如processes=4，起始端口为2900 则一般会使用2900 2901 2902 2903 4个端口作为内部通讯端口 多端口必改。
                'start_port' => 2910,


                /*以下数据为选填数据*/

                //ping 检测时间
                'ping_interval' => 55,
                //ping 检测 重试次数
                'ping_limit' =>1,
                // ping 发送内容
                //'ping_data' => '',

                /*以下数据为根据情况修改*/
                //回调类 可以写一下继承\GatewayChat\Contract\EventsInterface;的类
                'worker_event_handler'=> \GatewayChat\Events::class,
                // gateway进程数
                'processes'=>1,
                // 本机ip，分布式部署时使用内网ip
                'lan_ip'=> '127.0.0.1',
                //外部连接的IP地址  如是要外网可访问，须改
                'host'=>'0.0.0.0',
                // bussinessWorker进程数量 CPU核心数
                'worker_count' =>3,
            ],
            [
                'name'=>'chat_003',
                //外部连接的IP地址
                'host'=>'0.0.0.0',
                //外部连接端口
                'port'=>8203,
                //内部通信端口
                'bus_port'=>8103,

                // gateway进程数
                'processes'=>1,
                // 本机ip，分布式部署时使用内网ip
                'lan_ip'=> '127.0.0.1',
                // 内部通讯起始端口
                //假如gateway_count=4，起始端口为2900 则一般会使用2900 2901 2902 2903 4个端口作为内部通讯端口 多端口必改。
                'start_port' => 2940,
                //ping 检测时间
                'ping_interval' => 55,
                //ping 检测 重试次数
                'ping_limit' =>1,
                // *** ping 发送数据此处可以不设置***
                //'ping_data' => ''

                // bussinessWorker进程数量 CPU核心数
                'worker_count' =>1,
                //回调类
                'worker_event_handler'=> \GatewayChat\Events::class,

                /**
                 * 每个服务可以设定自己单独的数据库，以下必填
                 * */
                'database'=>[
                    // 服务器地址
                    'hostname'          => '127.0.0.1',
                    // 数据库名
                    'database'          => 'daixiaozhan',
                    // 用户名
                    'username'          => 'daixiaozhan',
                    // 密码
                    'password'          => 'Aa123456',
                    // 端口
                    'hostport'          => 3306,
                ],

            ]
        ]
    ]
];