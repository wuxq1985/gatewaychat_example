<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020-05-13
 * Time: 18:25
 */
return [
    'chat_001'=>[
        /**
         * 路由设置 ，在这里面设置的类，可以实现以下接口方法,不必都实现
         * \GatewayChat\Contract\OnCloseInterface
         * \GatewayChat\Contract\OnConnectInterface
         * \GatewayChat\Contract\OnMessageInterface
         * \GatewayChat\Contract\OnWebSocketConnectInterface
         * 设置的key即为路由
         * 设置的value即为操作方法，可以是字符串，也可以是数组，按上到下的顺序持行
         * *************
         * 新增回调判断
         * 如果 GatewayChat\Chat\Chat在 \app\websocket\Chat前面
         * GatewayChat\Chat\Chat::onMessage()返回假，则不会执行\app\websocket\Chat::onMessage()
         * 但如果 GatewayChat\Chat\Chat::class没有 onClose() ， 还是会执行 \app\websocket\Chat::onClose()
         * 即可以将上面一个方法做判断，是否进行下一个方法的执行
         * *************
         * */
        'chat'=>[
            /**
             * 如  \app\websocket\Chat::onMessage()返回假，则不会持行GatewayChat\Chat\Chat::onMessage()
             * */
            \app\websocket\Chat::class,
            GatewayChat\Chat\Chat::class,
        ],
        'ws8201/debug'=>[
            \app\websocket\Debug::class
        ],
        'bbs/cct'=>\app\websocket\Chat::class,
        //找不到路由时的操作，不建议将路径定为 wss://xx.cn/default
         'default'=>\app\websocket\Base::class,

        /**
         * 路由设置 ，公共操作,在下面传入的类,只实现
         * \GatewayChat\Contract\OnWorkerInterface
         * 的方法
         * */
        //onWorkerStart 他 也只能是在?base里面传入,只能实现 GatewayChat\Contract\OnWorkerInterface 的方法
        '?base'=>\app\websocket\Debug::class
    ],
    'chat_003'=>[
        'cctv'=>[
            \app\websocket\ChatOne::class,
        ]
    ]
];