## 安装 
环境要求，php>=7.2 mysql，redis 
依赖于workerman 需满足[workerman环境要求](http://doc.workerman.net/install/requirement.html)，暂不兼容window
~~~
git clone https://gitee.com/wuxq1985/gatewaychat_example.git 
cd gatewaychat_example
git clone https://gitee.com/wuxq1985/gatewaychat
composer install
composer dump-autoload -o
~~~

## 配置
修改 websocket_config里的配置 ，一般只建议修改下端口和路由,如无冲突，可以都不修改

修改好数据库配置

>需要数据库 mysql，redis 如redis键值有冲突可以修改 建议直接在redis中直接先择空数据库
```php
//  重写所有 GatewayChat\Chat\Chat::class 类的静态属性即可
```

## 运行
```
//直接以workerman的方式运行 
php chat.php start
//用thinkphp的方式运行
php think  chat start
//操作同workerman的gateway运行方式相同
```
## 路由
按示例所示，连接 ws://youhost.com:8201/chat 所有数据将先由 \app\websocket\Chat::class,类的方法处理，如返回为false,则不会进行下步操作
 